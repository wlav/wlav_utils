#include "wlav_utils/machine.h"

#include <iostream>
#include <sstream>
#include <string>

#include <mpi.h>
#include <dirent.h>
#include <execinfo.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sched.h>
#include <signal.h>

#include <mpi.h>


//- private helpers ---------------------------------------------------------
static void backtrace_printer(int signum, siginfo_t* info, void* ptr) {
     void* addresses[ 30 ];
     int c = backtrace( addresses, 30 );

     char** symbols = backtrace_symbols( addresses, c );
     for (int i = 0; i < c; i++) {
        std::cout << symbols[i] << std::endl;
     }

     free( symbols );
     exit(1);
}

static unsigned long hash_host( const std::string& buf ) {
    unsigned long h = 2166136261;
    for (std::string::size_type n = 0; n < buf.size(); ++n)
       h = (h * 16777619) ^ (unsigned long)buf[n];
    return h;
}

static FILE* open_ffile( int core_id ) {
   char fname[256];
   snprintf(fname, sizeof(fname), SCALING_SETFREQ_FILE_PATTERN, core_id );
   FILE* ffd = fopen( fname, "w" );
   if ( ffd == NULL )
      perror( fname );
   return ffd;
}

int wlav::MachineState::initialize() {
   if ( m_worldRank != -1 )
      return MPI_SUCCESS;

   int myhash = hash_host( hostname() );

   int result = MPI_Comm_rank( MPI_COMM_WORLD, &m_worldRank );
   if ( result != MPI_SUCCESS )
      return result;

   MPI_Comm localComm = MPI_COMM_NULL;
   result = MPI_Comm_split( MPI_COMM_WORLD, myhash, m_worldRank, &localComm );
   if ( result != MPI_SUCCESS )
      return result;

   result = MPI_Comm_rank( localComm, &m_localRank );
   if ( result != MPI_SUCCESS ) {
      MPI_Comm_free( &localComm );
      return result;
   }

   result = MPI_Comm_size( localComm, &m_localSize );

   MPI_Comm_free( &localComm );

   return result;
}


//---------------------------------------------------------------------------
wlav::MachineState::MachineState() {
   m_worldRank = -1;
   m_localRank = -1;
   m_localSize = -1;
   m_coreid    = -1;
   for (int i = 0; i < HYPERTHREADS; ++i) m_coreff[i] = NULL;

   struct sigaction segact;
   segact.sa_sigaction = backtrace_printer;
   segact.sa_flags = SA_SIGINFO;
   sigaction(SIGSEGV, &segact, NULL);
}


//---------------------------------------------------------------------------
wlav::MachineState::~MachineState() {
   for ( std::vector<void*>::iterator ff = m_ffreqs.begin(); ff != m_ffreqs.end(); ++ff )
      fclose( (FILE*)*ff );
   for (int i = 0; i < HYPERTHREADS; ++i) {
      if ( m_coreff[i] ) fclose( (FILE*)m_coreff[i] );
   }
}


//---------------------------------------------------------------------------
wlav::MachineState& wlav::MachineState::state() {
   static MachineState m;
   return m;
}


//---------------------------------------------------------------------------
const std::string& wlav::MachineState::hostname() {
   if ( m_hostname.empty() ) {
      char hn[HOST_NAME_MAX];
      if ( gethostname( hn, HOST_NAME_MAX ) != 0 )
         m_hostname = "<unknown>";
      else
         m_hostname = hn;
   }
   return m_hostname;
}


//---------------------------------------------------------------------------
int wlav::MachineState::worldRank() {
   if ( initialize() != MPI_SUCCESS )
      return -1;

   return m_worldRank;
}


//---------------------------------------------------------------------------
int wlav::MachineState::localRank() {
   if ( initialize() != MPI_SUCCESS )
      return -1;

   return m_localRank;
}


//---------------------------------------------------------------------------
int wlav::MachineState::localSize() {
   if ( initialize() != MPI_SUCCESS )
      return -1;

   return m_localSize;
}


//---------------------------------------------------------------------------
bool wlav::MachineState::amLocalMaster() {
   if ( initialize() != MPI_SUCCESS )
      return false;

   return m_localRank == 0;
}


//---------------------------------------------------------------------------
int wlav::MachineState::setAffinity( EAffinityPlan eap ) {
/* HT count is 0..NUMCORES-1         for HT1
               NUMCORES...2*NUMCORES for HT2 */

   cpu_set_t myset;
   CPU_ZERO( &myset );
// divide compactly over each socket
   if ( eap == compact ) {
      m_coreid = (localRank() < localSize()/NUMSOCKETS) ?
           localRank() : localRank() - localSize()/NUMSOCKETS + NUMCORES/NUMSOCKETS;
   } else /* scatter */
      m_coreid = int( NUMCORES / double(localSize()) ) * localRank();
   CPU_SET( m_coreid, &myset );
   sched_setaffinity( 0, sizeof(cpu_set_t), &myset );

   int test_id = -1;
   while (1) {
      test_id = sched_getcpu();

      bool affOk = test_id == m_coreid
#if HYPERTHREADS == 1
      ;
#elif HYPERTHREADS == 2
      || test_id == 2 * m_coreid;
#else
#error "only HYPERTHREADS == 1 || == 2 handled"
      ;
#endif
      if ( ! affOk ) sched_yield();
      else break;
   }
}


//---------------------------------------------------------------------------
int wlav::MachineState::setNodeFreq( unsigned int freq, bool verify ) {
#ifdef SET_FREQUENCY
    if ( initialize() != MPI_SUCCESS )
       return false;

    if ( m_ffreqs.empty() ) {
       const int NFILES = NUMCORES * HYPERTHREADS;
       int count = (m_localSize > 1) ? NFILES / (m_localSize - 1) : 0;

       if ( m_localRank != 0 ) {
          int start = (m_localRank - 1) * count;

          for ( int core_id = start; core_id < start + count; ++core_id ) {
             FILE* ffd = open_ffile( core_id );
             if ( ffd == NULL ) return 1;
             m_ffreqs.push_back( ffd );
          }
       } else {
          int remainder = (m_localSize > 1) ? NFILES % (m_localSize - 1) : NFILES;
          for ( int core_id = NFILES - remainder; core_id < NFILES; ++core_id ) {
             FILE* ffd = open_ffile( core_id );
             if ( ffd == NULL ) return 1;
             m_ffreqs.push_back( ffd );
          }
       }
    }

    if (freq == highest)
       freq = TOP_FREQUENCY;
    else if (freq == max_noturbo)
       freq = MAX_NOTURBO;
    else if (freq == lowest) 
       freq = BOTTOM_FREQUENCY;

    for ( std::vector<void*>::iterator ff = m_ffreqs.begin(); ff != m_ffreqs.end(); ++ff ) {
       if ( fprintf( (FILE*)*ff, "%u", freq ) < 0 ) {
          perror( "Failed to set frequency" );
          return 1;
       }
       fflush( (FILE*)*ff );
    }

    if ( verify ) {
       MPI_Barrier( MPI_COMM_WORLD );

       if ( m_localRank == 0 ) {
          for ( int core_id = 0; core_id < NUMCORES * HYPERTHREADS; ++core_id ) {
             int curfreq;
             char fname[256];
             snprintf( fname, sizeof(fname), SCALING_SETFREQ_FILE_PATTERN, core_id );
             FILE* f = fopen(fname, "r");
             fscanf( f, "%d", &curfreq );
             fclose( f );
             printf( "set frequency to %d for %d (intended: %d)\n", curfreq, core_id, freq );
          }
       }
    }

#endif
    return 0;
}


//---------------------------------------------------------------------------
int wlav::MachineState::setCoreFreq( unsigned int freq ) {
#ifdef SET_FREQUENCY
    if ( initialize() != MPI_SUCCESS )
       return false;

    if ( m_coreid == -1 )
       setAffinity();

    if (freq == highest)
       freq = TOP_FREQUENCY;
    else if (freq == lowest)
       freq = BOTTOM_FREQUENCY;

    for (int i = 0; i < HYPERTHREADS; ++i) {
       if ( m_coreff[i] == NULL ) {
          m_coreff[i] = open_ffile( m_coreid * (i+1) );
          if ( ! m_coreff[i] ) return 1;
       }

       if ( fprintf( (FILE*)m_coreff[i], "%u", freq ) < 0 ) {
          perror( "Failed to set frequency" );
          return 1;
       }
       fflush( (FILE*)m_coreff[i] );
    }
#endif
    return 0;
}


//---------------------------------------------------------------------------
int wlav::MachineState::localOnce(int (*callback)() ) {
   if ( amLocalMaster() )
      return callback();

   return MPI_SUCCESS;
}


//---------------------------------------------------------------------------
extern "C" int wlav_setAffinity() {
   return wlav::MachineState::state().setAffinity();
}

//---------------------------------------------------------------------------
extern "C" int wlav_setNodeFreq( unsigned int freq ) {
   return wlav::MachineState::state().setNodeFreq( freq );
}

//---------------------------------------------------------------------------
extern "C" int wlav_setCoreFreq( unsigned int freq ) {
   return wlav::MachineState::state().setCoreFreq( freq );
}

