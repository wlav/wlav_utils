#include <string>
#include <vector>

#define SET_AFFINITY      1

#if defined(EDISON_)
//#define USE_RAPL          1
#define SAFE_RAPL         1
#define USE_DMAPP         1
#define NUMCORES         24
#define NUMSOCKETS        2
#define HYPERTHREADS      1
#define CPUSCALE          1
#define MEMSCALE          2

#elif defined(CORI_)
#define NUMCORES         32
#define NUMSOCKETS        2
#define HYPERTHREADS      2
#define CPUSCALE          1
#define MEMSCALE          2

#elif defined(BABBAGE_)
#define NUMCORES         16
#define NUMSOCKETS        2
#define HYPERTHREADS      2
#define CPUSCALE          1
#define MEMSCALE          2

#elif defined(SHEPARD_)
#define USE_GASNET        1
#define USE_PIPROXY       1
#define SET_FREQUENCY     1
#define NUMCORES         32
#define NUMSOCKETS        2
#define HYPERTHREADS      2
#define CPUSCALE          4
#define MEMSCALE          5
#define TOP_FREQUENCY     2301000
#define MAX_NOTURBO       2300000
#define BOTTOM_FREQUENCY  1200000

#elif defined(CEREBRO_)
#define USE_GASNET        1
#define NUMCORES         16
#define NUMSOCKETS        2
#define HYPERTHREADS      2

#else
#error "UNKOWN MACHINE"
#endif

#define SCALING_SETFREQ_FILE_PATTERN "/sys/devices/system/cpu/cpu%u/cpufreq/scaling_setspeed"


namespace wlav {

enum EFrequencyRange : unsigned int { lowest = 1, max_noturbo = 9, highest = 10 };
enum EAffinityPlan : unsigned int { compact = 1, scatter = 2 };

class MachineState {
   MachineState();
   ~MachineState();
public:
   static MachineState& state();

// information
   const std::string& hostname();

   int worldRank();
   int localRank();
   int localSize();

   bool amLocalMaster();

// work
   int setAffinity( EAffinityPlan eap = compact );
   int setNodeFreq( unsigned int freq, bool verify = false );
   int setCoreFreq( unsigned int freq );
   int localOnce( int (*)() );

private:
   int initialize();

private:
   std::string m_hostname;
   std::vector< void* > m_ffreqs;
   int m_worldRank;
   int m_localRank;
   int m_localSize;
   int m_coreid;
   void* m_coreff[HYPERTHREADS];
};

} // namespace wlav
