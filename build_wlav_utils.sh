#!/bin/sh
CFLAGS="-O3 -g -std=c++14 -fno-omit-frame-pointer -pthread -fpic -Iwlav_utils"

CXX=mpiicc
if [ "$NERSC_HOST" == "edison" ]; then
   CXX=CC
   CFLAGS="$CFLAGS -DEDISON_=1"
elif [ "$NERSC_HOST" == "cori" ]; then
   CXX=CC
   CFLAGS="$CFLAGS -DCORI_=1"
elif [ "$NERSC_HOST" == "babbage" ]; then
   CFLAGS="$CFLAGS -DBABBAGE_=1"
elif [ "$NERSC_HOST" == "shepard" ]; then
   NERSC_HOST=shepard
   CC=gcc
   CXX=mpicxx
   CFLAGS="$CFLAGS -DSHEPARD_=1"
else
# assume cerebro
   NERSC_HOST=cerebro
   CC=gcc
   CXX=mpicxx
   CFLAGS="$CFLAGS -DCEREBRO_=1"
fi

set -ex

$CXX -c $CFLAGS machine.cxx
$CXX -c $CFLAGS piproxy.cxx
ar -r libwlav_utils-$NERSC_HOST.a machine.o piproxy.o
