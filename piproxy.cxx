#include "wlav_utils/machine.h"
#include "wlav_utils/piproxy.h"

#include <iostream>

#include <sys/types.h>

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>


//- private helpers ---------------------------------------------------------
static bool manage_piproxy( FILE* output ) {
   int fd_out = fileno( output );

   fd_set readset;
   FD_ZERO( &readset );
   FD_SET( fd_out, &readset );

   struct timeval tv;
   tv.tv_sec  = 10;
   tv.tv_usec =  0;

   int result = select( fd_out + 1, &readset, NULL, NULL, &tv );
   if ( result == -1 ) {
      perror( "Failed to read from piproxy process" );
      return false;
   }

   const std::string& hn = wlav::MachineState::state().hostname();

   if ( result == 0 ) {
      std::cerr << "timeout while reading piproxy on " << hn << std::endl;
      FILE* pidtokill = popen( "pgrep piproxy", "r" );
      char buf[128];
      if ( fgets(buf, 128, pidtokill) != NULL ) { kill( atoi( buf ), SIGKILL ); }
      return false;
   }

   const int bufsize = 1024;
   char buf[bufsize]; buf[bufsize-1] = '\0';
   while ( fgets( buf, bufsize, output ) != NULL ) {
      if ( strstr( buf, "connect: No route to host"   ) ||
           strstr( buf, "connect: Connection refused" ) ) {
         return false;
      }
      std::cout << hn << ": " << buf << std::flush;
   }
   return true;
}


//---------------------------------------------------------------------------
bool wlav::piproxy_reset() {
#ifdef USE_PIPROXY
   MachineState& state = MachineState::state();
   if ( state.amLocalMaster() ) { 
      const std::string& hostname = state.hostname();
      int myrank = state.worldRank();

      char cmdbuf[64];
      snprintf(cmdbuf, 64, "piproxy -a 10.101.12.1%s -t 0 -r 2>&1", hostname.c_str()+4);
      std::cout << "now running " << cmdbuf << " by " << myrank << " on " << hostname << std::endl;
      FILE* output = popen( cmdbuf, "r" );
      if ( output == NULL ) {
         std::cerr << " ... failure starting (reset) piproxy on " << hostname << std::endl;
         return false;
      }

      if ( ! manage_piproxy( output ) ) {
         std::cerr << " ... failure running (reset) piproxy on " << hostname << std::endl;
         pclose( output );
         return false;
      }

      std::cout << " ... resetting done on " << hostname << std::endl;
      pclose( output );
      return true;
   }
#endif
   return false;
}


//---------------------------------------------------------------------------
bool wlav::piproxy_report() {
#ifdef USE_PIPROXY
   MachineState& state = MachineState::state();
   if ( state.amLocalMaster() ) {
      const std::string& hostname = state.hostname();
      int myrank = state.worldRank();

      char cmdbuf[64];
      snprintf(cmdbuf, 64, "piproxy -a 10.101.12.1%s -t 0 -c 2>&1", hostname.c_str()+4);
      std::cout << "now running " << cmdbuf << " by " << myrank << " on " << hostname << std::endl;
      FILE* output = popen( cmdbuf, "r" );
      if ( output == NULL ) {
         std::cerr << " ... failure starting (readout) piproxy on " << hostname << std::endl;
         return false;
      }

      if ( ! manage_piproxy( output ) ) {
         std::cerr << " ... failure running (readout) piproxy on " << hostname << std::endl;
         pclose( output );
         return false;
      }

      std::cout << " ... sampling done on " << hostname << std::endl;
      pclose( output );
      return true;
   }
#endif
   return false;
}


//---------------------------------------------------------------------------
extern "C" void wlav_piproxy_reset_() {
   wlav::piproxy_reset();
}

//---------------------------------------------------------------------------
extern "C" void wlav_piproxy_report_() {
   wlav::piproxy_report();
}
